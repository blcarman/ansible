Py3web
======

A basic role to set up a web server to host a django application using the Python WSGI module and apache. It does not currently set up the django project, but after the role has been applied you can activate the python virtualenv and run django-admin myapp . in the site_path to create. After creating the django project edit the settings.py and add your vhost in the AllowedHosts option. This functionality may be built directly into the role at some point in the future.

Requirements
------------

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

virtualhost name
vhostname:
site_path: /opt/www/{{ vhostname }}
python virtual environment requirements
requirements_file: "{{ site_path }}/requirements.txt"
django_project: myapp
django_project_path: "{{ site_path }}/{{ django_project }}"
wsgi_script: wsgi.py
virtual_env_path: "{{ site_path }}/py3venv"

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
