Role Name
=========

Basic role for configured a central borg backup server

Requirements
------------

borgbackup
disk with enough space to store your backups is already formatted

Role Variables
--------------

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

MIT