#!pwsh

$ANSIBLE_NAMESPACE_DIR="ansible_collections/lab"

# determine which collections have been modified
$BRANCH=$(git symbolic-ref -q HEAD)

# alternatively we could get all files with git diff master...$BRANCH --name-only
# get the changed dirs, limit to just ansible collecitons, strip off the percent changed, collection name is reliably the 3rd directory, remove duplicates due to collections having multiple directories
$CHANGED_COLLECTIONS=$(git diff master...$BRANCH --dirstat=files,0 | grep ${ANSIBLE_NAMESPACE_DIR} | awk '{print $2}' | awk -F / '{print $3}') | get-unique

# if no collections changed exit immediately
if ($CHANGED_COLLECTIONS)
{
  write-host "no changes, exiting"
  exit 0
}

write-host $CHANGED_COLLECTIONS
foreach ($COLLECTION in $CHANGED_COLLECTIONS)
{
  write-host $COLLECTION
  # $VERSION_CHANGED = $(git diff master -- $ANSIBLE_NAMESPACE_DIR/$COLLECTION/galaxy.yml | grep "[+-]version")
  # TODO: check if galaxy.yml version has changed
  $PREVIOUS_VERSION=$(grep version: $ANSIBLE_NAMESPACE_DIR/$COLLECTION/galaxy.yml | cut -d " " -f 2).split(".")

  if ($(git log master...$BRANCH --grep='BREAKING CHANGE' --regexp-ignore-case))
  {
    write-host "breaking change"
    $PREVIOUS_VERSION[0] = [int]$PREVIOUS_VERSION[0] + 1
    $PREVIOUS_VERSION[1] = 0
    $PREVIOUS_VERSION[2] = 0
  }
  elseif ($BRANCH.startswith('refs/heads/f'))
  {
    write-host "feature"
    $PREVIOUS_VERSION[1] = [int]$PREVIOUS_VERSION[1] + 1
    $PREVIOUS_VERSION[2] = 0
  }
  elseif ($BRANCH.startswith('refs/heads/b'))
  {
    write-host "bugfix"
    $PREVIOUS_VERSION[2] = [int]$PREVIOUS_VERSION[2] + 1
  }
  write-host "previous version: $PREVIOUS_VERSION"
  $NEW_VERSION = $PREVIOUS_VERSION -join "."
  write-host "new version: $NEW_VERSION"
  sed -i "s/version:.*/version: ${NEW_VERSION}/g" $ANSIBLE_NAMESPACE_DIR/$COLLECTION/galaxy.yml
  git add $ANSIBLE_NAMESPACE_DIR/$COLLECTION/galaxy.yml
  git commit -m "bumping version of $COLLECTION"
}

# check branch name and commit messages for version change info
# determine updated version number based on branch name and commit messages

# update version number if it will be greater than what it's already set to (this might need some finesse, 
# that is if I manually update the version I don't necessarily want the script undoing that... 
# unless the script would bump the version higher than I did).